﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EL_AMRI
{
    public abstract class Dipendenti
    {
        private string _matricola;
        private string _nominativo;
        protected bool _haTempoIndeterminato;
        private double _orelavorate;
        private double _costoorario;

        DateTime datetime = new DateTime(2017, 1, 18);


        public string Matricola
        {
            get { return _matricola; }
            set { _matricola = value; }
        }

        public string Nominativo
        {
            get { return _nominativo; }
            set { _nominativo = value; }
        }

        public bool HaTempoInd
        {
            get { return _haTempoIndeterminato; }
            set { _haTempoIndeterminato = value; }
        }

        public double OreLavorate
        {
            get { return _orelavorate; }
            set { _orelavorate = value; }
        }

        public double CostoOrario
        {
            get { return _costoorario; }
            set { _costoorario = value; }
        }

        public double CalcolaSalario( double _orelavorate, double _costoorario)
        {
            return 0;
        }
    }

    public class Operaio : Dipendenti
    {
        protected double _salario;

        public double Salario
        {
            get { return _salario; }
            set { _salario = value; }
        }
        public double CalcolaSalario(double _orelavorate, double _costoorario, double _salario)
        {
            _salario = _orelavorate * _costoorario;
            if (_orelavorate >= 160)
            {
                _salario = 160 * _costoorario + ((_orelavorate - 160) * _costoorario) * 1.1;
            }

            return _salario;
        }
    }
    public class Impiegato : Dipendenti
    {
        protected double _salario;

        public double Salario
        {
            get { return _salario; }
            set { _salario = value; }
        }

        public double CalcolaSalario(double _orelavorate, double _costoorario, double _salario)
        {
            _salario = 160 * _costoorario;
            if (_orelavorate >= 160)
            {
                _salario = _salario + ((_orelavorate - 160) * _costoorario) * 1.2;
            }

            return _salario;
        }
    }
    public class Manager : Dipendenti
    {
        protected double _salario;

        public double Salario
        {
            get { return _salario; }
            set { _salario = value; }
        }

        public double CalcolaSalario(double _orelavorate, double _costoorario, double _salario)
        {
            _salario = _orelavorate * _costoorario;
            if (_orelavorate >= 160)
            {
                _salario = _salario /*+ 5% della sommatoria salari dipendenti non dirigenti*/;
            }

            return _salario;
        }
    }
}
